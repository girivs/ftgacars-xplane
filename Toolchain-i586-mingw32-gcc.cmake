# the name of the target operating system
SET(CMAKE_SYSTEM_NAME Windows)
#SET(TARGET_CC i586-mingw32-gcc-4.8) 
#SET(TARGET_CXX i586-mingw32-g++-4.8)
# Choose an appropriate compiler prefix

# for classical mingw32
# see http://www.mingw.org/
set(COMPILER_PREFIX "i586-mingw32")
SET(CMAKE_FIND_ROOT_PATH /usr/local/gcc-4.8.0-qt-4.8.4-for-mingw32/win32-gcc ${PROJECT_SOURCE_DIR})
# for 32 or 64 bits mingw-w64
# see http://mingw-w64.sourceforge.net/
#set(COMPILER_PREFIX "i686-w64-mingw32")
#set(COMPILER_PREFIX "x86_64-w64-mingw32"

# which compilers to use for C and C++
find_program(CMAKE_RC_COMPILER NAMES ${COMPILER_PREFIX}-windres)
SET(CMAKE_RC_COMPILER ${COMPILER_PREFIX}-windres)
find_program(CMAKE_C_COMPILER NAMES ${COMPILER_PREFIX}-gcc)
SET(CMAKE_C_COMPILER ${COMPILER_PREFIX}-gcc)
find_program(CMAKE_CXX_COMPILER NAMES ${COMPILER_PREFIX}-g++)
SET(CMAKE_CXX_COMPILER ${COMPILER_PREFIX}-g++)

# here is the target environment located
#SET(USER_ROOT_PATH /home/girivs/src/win32-dev)




# adjust the default behaviour of the FIND_XXX() commands:
# search headers and libraries in the target environment, search 
# programs in the host environment
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)