/*
 * ftgacars.cpp
 * 
 * Created by Shankar Giri V on 07/20/2013.
 * 
 * Copyright (c) 2013
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * Neither the name of the project's author nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <string.h>
#include "XPLMMenus.h"
#include "XPWidgets.h"
#include "XPStandardWidgets.h"
#include "XPLMNavigation.h"
#include "XPLMProcessing.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "XPLMPlugin.h"
#include "FTGACARSConfig.h"

/* File to write data to. */
FILE *  gOutputFile;

/* Data refs we will record. */
XPLMDataRef     gPlaneZulu;
XPLMDataRef     gPlaneLat;
XPLMDataRef     gPlaneLon;
XPLMDataRef     gPlaneEl;
XPLMDataRef     gPlaneYAGL;
XPLMDataRef     gPlaneVVI;
XPLMDataRef     gPlaneIAS;
XPLMDataRef     gPlaneTAS;
XPLMDataRef     gPlaneGndSpeed;
XPLMDataRef     gPlaneNumEngines;
XPLMDataRef     gPlaneEngRun;
XPLMDataRef     gPlaneFuelFlow;
XPLMDataRef     gPlaneLocalTimeSeconds;
XPLMDataRef     gPlaneAircraftICAO;
XPLMDataRef     gPlaneAircraftTailNum;
XPLMDataRef     gPlaneGearForces;
XPLMDataRef     gPlaneParkBrake;

float gPrevLat = 0.0f;
float gPrevLon = 0.0f;
float gLandingVVI = 0.0f;
float gLandingIAS = 0.0f;
float gGndSpeed = 0.0f;
float gMaxElevation = 0.0f;
float gCruiseElevation = 0.0f;
float gCruiseMach = 0.0f;
float gGateStartTimer = 0.0f;
float gGateStopTimer = 0.0f;
float gAirborneStartTimer = 0.0f;
float gAirborneStopTimer = 0.0f;
float gFuelFlow[9];
float gFuelUsed=0.0f;
float gDistance = 0.0f;
float gDepartureLocalTime;
float gShutdownLocalTime;
float gAirborneLocalTime;
float gTouchDownLocalTime;
int   gDebounceCounter = 0;
int   gAverageSamples = 0;
int   gNumEngines = 0;
char gsDepartureICAO[32]="\0";
char gsDestinationICAO[32]="\0";
char gPID[6]="\0", gFlightNum[16]="\0", gFlightReg[16]="\0";

typedef enum {  INIT      = 0, 
                IDLE      = 1, 
                START     = 2, 
                GATE_DEP  = 3, 
                TAKEOFF   = 4, 
                CLIMB     = 5, 
                CRUISE    = 6, 
                UNDEFINED = 7, 
                DESCEND   = 8, 
                APPROACH  = 9, 
                TOUCHDOWN = 10, 
                GATE_ARR  = 11, 
                SHUTDOWN  = 12
            }  stateEnum;
static const char* enumStrings[] = {"INIT", "IDLE", "START", "GATE_DEP",
                                    "TAKEOFF", "CLIMB", "CRUISE", "UNDEFINED",
                                    "DESCEND", "APPROACH", "TOUCHDOWN", "GATE_ARR", 
                                    "SHUTDOWN"};
static stateEnum gCurrentState = INIT, gNewState = INIT; 

static int MenuItem1;
static XPWidgetID FTGACARSWidget=NULL;
static XPWidgetID PIDEdit, FlightNumEdit, FlightRegEdit;
static XPWidgetID PIDCaption, FlightNumCaption, FlightRegCaption;

static void FTGACARSMenuHandler(void *, void *);
static void CreateFTGACARSWidget(int x1, int y1, int w, int h);
static int FTGACARSHandler(
                        XPWidgetMessage         inMessage,
                        XPWidgetID              inWidget,
                        intptr_t                inParam1,
                        intptr_t                inParam2);
                        

float   MyFlightLoopCallback(
                                   float                inElapsedSinceLastCall,    
                                   float                inElapsedTimeSinceLastFlightLoop,    
                                   int                  inCounter,    
                                   void *               inRefcon);    


float CalculateFuelBurn(float FuelFlow[]) {
    float FuelBurn = 0.0f;
    /* Calculate fuel burn from fuel flows */   
    for (int i=0;i<9;i++)      
        FuelBurn += FuelFlow[i];
    return FuelBurn;
}

int AreEnginesStopped(void) {
    int engineRunArray[8];
    int enginesStopped = 1;
    XPLMGetDatavi(gPlaneEngRun, engineRunArray, 0, gNumEngines);
    for (int i=0;i<gNumEngines; i++) {
        if (engineRunArray[i] != 0) {
            enginesStopped = 0;
            break;
        }
    }
    return enginesStopped;
}

int DebounceElevation(float fElevation) {
    if (gMaxElevation < fElevation) gMaxElevation = fElevation;
    if (((gMaxElevation - fElevation) < 120) && (fabs(XPLMGetDataf(gPlaneVVI)) < 300.0f))
        gDebounceCounter++;
    else
        gDebounceCounter = 0;
    return gDebounceCounter;
}

float BlkTimeinDecimals(float start, float stop) {
    int time;
    if (start > stop)
        time = (86400 - start) + stop;
    else
        time = stop - start;
    time = (time/60)+6;
    int   timehrs = int(time / 60);
    int   timemin = time % 60;
    return (timehrs + (int(timemin/6)/10.0));
}

/* Calculations from Earth Atmospheric Model (Metric) from NASA */
/* http://www.grc.nasa.gov/WWW/k-12/airplane/atmosmet.html */
float MachfromTAS(float el, float tas) {
    float T;
    if (el < 11000) /* Below tropopause */
            T = 288.19 - 0.00649 * el;
    else if (el < 25000) /* At lower stratosphere */
            T = 216.69;
    else /* At lower stratosphere */
            T = 141.94 + .00299 * el;
    float a = 20.05173 * sqrt(T);
    return (tas / a);
}

void FindNearestAirport(float lat, float lon, char* icao) {
    XPLMNavRef  navRef     = XPLMFindNavAid(
                                   NULL,    /* Can be NULL */
                                   NULL,    /* Can be NULL */
                                   &lat,    /* Can be NULL */
                                   &lon,    /* Can be NULL */
                                   NULL,    /* Can be NULL */
                                   xplm_Nav_Airport);
    if (navRef != XPLM_NAV_NOT_FOUND) {
        XPLMGetNavAidInfo(
                                   navRef,    
                                   NULL,    /* Can be NULL */
                                   NULL,    /* Can be NULL */
                                   NULL,    /* Can be NULL */
                                   NULL,    /* Can be NULL */
                                   NULL,    /* Can be NULL */
                                   NULL,    /* Can be NULL */
                                   icao,    /* Can be NULL */
                                   NULL,    /* Can be NULL */
                                   NULL);    /* Can be NULL */
    }
            

}

PLUGIN_API int XPluginStart(
                        char *      outName,
                        char *      outSig,
                        char *      outDesc)
{
    
    XPLMMenuID  id;
    int         item;
    // Create our menu
    item = XPLMAppendMenuItem(XPLMFindPluginsMenu(), "FTGACARS", NULL, 1);
    id = XPLMCreateMenu("FTGACARS", XPLMFindPluginsMenu(), item, FTGACARSMenuHandler, NULL);
    XPLMAppendMenuItem(id, "FTGACARS Setup", (void *)"FTGACARS", 1);

    // Flag to tell us if the widget is being displayed.
    MenuItem1 = 0;  
    char strVersion[16]="\0";
    sprintf(strVersion, "Version %d.%d.%d", FTGACARS_VERSION_MAJOR, FTGACARS_VERSION_MINOR, FTGACARS_VERSION_REVISION);
    strcpy(outName, "FTGACARS");
    strcpy(outSig, "ftg.alpha.ftgacars");
    strcpy(outDesc, "A plugin for FTG ACARS. ");
    strcat(outDesc, strVersion);

    gOutputFile = fopen("ftgacars.txt", "w");

    /* Find the data refs we want to record. */
    gPlaneZulu = XPLMFindDataRef("sim/time/zulu_time_sec");
    gPlaneLat = XPLMFindDataRef("sim/flightmodel/position/latitude");
    gPlaneLon = XPLMFindDataRef("sim/flightmodel/position/longitude");
    gPlaneEl = XPLMFindDataRef("sim/flightmodel/position/elevation");
    gPlaneYAGL = XPLMFindDataRef("sim/flightmodel/position/y_agl");
    gPlaneVVI = XPLMFindDataRef("sim/flightmodel/position/vh_ind_fpm");
    gPlaneIAS = XPLMFindDataRef("sim/flightmodel/position/indicated_airspeed");
    gPlaneTAS     = XPLMFindDataRef("sim/flightmodel/position/true_airspeed");
    gPlaneGndSpeed = XPLMFindDataRef("sim/flightmodel/position/groundspeed");
    gPlaneNumEngines = XPLMFindDataRef("sim/aircraft/engine/acf_num_engines");
    gPlaneEngRun = XPLMFindDataRef("sim/flightmodel/engine/ENGN_running");
    gPlaneFuelFlow = XPLMFindDataRef("sim/flightmodel/engine/ENGN_FF_");
    gPlaneLocalTimeSeconds = XPLMFindDataRef("sim/time/local_time_sec");
    gPlaneAircraftICAO = XPLMFindDataRef("sim/aircraft/view/acf_ICAO");
    gPlaneAircraftTailNum = XPLMFindDataRef("sim/aircraft/view/acf_tailnum");
    gPlaneGearForces = XPLMFindDataRef("sim/flightmodel/forces/faxil_gear");
    gPlaneParkBrake = XPLMFindDataRef("sim/flightmodel/controls/parkbrake");
    /* Register our callback for once a second.  Positive intervals
     * are in seconds, negative are the negative of sim frames.  Zero
     * registers but does not schedule a callback for time. */
    XPLMRegisterFlightLoopCallback(     
            MyFlightLoopCallback,   /* Callback */
            1.0,                    /* Interval */
            NULL);                  /* refcon not used. */
            
    return 1;
}

PLUGIN_API void XPluginStop(void)
{
    /* Unregister the callback */
    XPLMUnregisterFlightLoopCallback(MyFlightLoopCallback, NULL);
    if (MenuItem1 == 1)
    {
        XPDestroyWidget(FTGACARSWidget, 1);
        MenuItem1 = 0;
    }
    float blockTime = BlkTimeinDecimals(gGateStartTimer, gGateStopTimer);
    float airTime = BlkTimeinDecimals(gAirborneStartTimer, gAirborneStopTimer);
    char airICAO[5];
    //char tailNum[8];
    XPLMGetDatab(gPlaneAircraftICAO, airICAO, 0, 8);
    //XPLMGetDatab(gPlaneAircraftTailNum, tailNum, 0, 8);
    if (gCruiseElevation == 0.0f) {
        gCruiseElevation = gMaxElevation/10.0f;
        gAverageSamples = 1;
     }
    /* Record all flight parameters at the end of the flight */
    fprintf(gOutputFile, "PID= %s\n", gPID); 
    fprintf(gOutputFile, "Departure ICAO= %s\n", gsDepartureICAO); 
    fprintf(gOutputFile, "Destination ICAO= %s\n", gsDestinationICAO); 
    fprintf(gOutputFile, "Aircraft ICAO= %s\n", airICAO); 
    fprintf(gOutputFile, "Flight Number= %s\n", gFlightNum); 
    fprintf(gOutputFile, "Aircraft Reg.= %s\n", gFlightReg); 
    fprintf(gOutputFile, "Distance= %u NM.\n", int(round(gDistance/1852.0)));
    fprintf(gOutputFile, "Flight Level=FL %03u.\n", int(round((gCruiseElevation/gAverageSamples)*0.328084)));
    fprintf(gOutputFile, "Cruise Speed=Mach %5.2f.\n", gCruiseMach/gAverageSamples);
    fprintf(gOutputFile, "Landing IAS=%03u kts.\n", int(round(gLandingIAS)));
    fprintf(gOutputFile, "Landing VVI=%03d fpm.\n", int(round(gLandingVVI)));
    fprintf(gOutputFile, "Gate departure local time=%02d%02d.\n", int(gDepartureLocalTime / 3600), int(int(gDepartureLocalTime) % 3600 / 60));
    fprintf(gOutputFile, "Airborne local time=%02d%02d.\n", int(gAirborneLocalTime / 3600), int(int(gAirborneLocalTime) % 3600 / 60));
    fprintf(gOutputFile, "Touchdown local time=%02d%02d.\n", int(gTouchDownLocalTime / 3600), int(int(gTouchDownLocalTime) % 3600 / 60));
    fprintf(gOutputFile, "Engine shutdown local time=%02d%02d.\n", int(gShutdownLocalTime / 3600), int(int(gShutdownLocalTime) % 3600 / 60));
    fprintf(gOutputFile, "Block time=%f.\n", blockTime);
    fprintf(gOutputFile, "Flight time=%f.\n", airTime);
    fprintf(gOutputFile, "Total fuel used=%d lbs.\n", int(round(gFuelUsed*2.20462262)));
    /* Close the file */
    fclose(gOutputFile);
}

PLUGIN_API void XPluginDisable(void)
{
    /* Flush the file when we are disabled.  This is convenient; you 
     * can disable the plugin and then look at the output on disk. */
    fflush(gOutputFile);
}

PLUGIN_API int XPluginEnable(void)
{
    return 1;
}

PLUGIN_API void XPluginReceiveMessage(
                    XPLMPluginID    inFromWho,
                    int         inMessage,
                    void *          inParam)
{
     if (inFromWho == XPLM_PLUGIN_XPLANE) {
        if (inMessage == XPLM_MSG_PLANE_LOADED) {
            if (*((int*)(&inParam)) == XPLM_PLUGIN_XPLANE)
                gNumEngines = XPLMGetDatai(gPlaneNumEngines);
                if (gNumEngines == 0) gNumEngines = 1;
                gNewState = INIT;
        }
        if (*((int*)(&inParam)) == XPLM_MSG_AIRPORT_LOADED)
            gNewState = INIT;
    }
}

// This will create our widget from menu selection.
// MenuItem1 flag stops it from being created more than once.
void FTGACARSMenuHandler(void * mRef, void * iRef)
{
    // If menu selected create our widget dialog
    if (!strcmp((char *) iRef, "FTGACARS"))
    {
        if (MenuItem1 == 0)
        {
            CreateFTGACARSWidget(221, 640, 420, 290);
            MenuItem1 = 1;
        }
        else
            if(!XPIsWidgetVisible(FTGACARSWidget))
                XPShowWidget(FTGACARSWidget);
    }
}

// This will create our widget dialog.
// I have made all child widgets relative to the input paramter.
// This makes it easy to position the dialog
void CreateFTGACARSWidget(int x, int y, int w, int h)
{
    int x2 = x + w;
    int y2 = y - h;
    char Buffer[255];
#if (FTGACARS_VERSION_MINOR&0x01)
    const char* cBuildType = "(Development Build)";
#else
    const char* cBuildType = "(Stable Build)";  
#endif
    sprintf(Buffer, "FTGACARS Setup Version %d.%d.%d %s", FTGACARS_VERSION_MAJOR, FTGACARS_VERSION_MINOR, FTGACARS_VERSION_REVISION, cBuildType);
    // Create the Main Widget window
    FTGACARSWidget = XPCreateWidget(x, y, x2, y2,
                    1,  // Visible
                    Buffer, // desc
                    1,      // root
                    NULL,   // no container
                    xpWidgetClass_MainWindow);

    // Add Close Box decorations to the Main Widget
    XPSetWidgetProperty(FTGACARSWidget, xpProperty_MainWindowHasCloseBoxes, 1);

    PIDCaption = XPCreateWidget(x+80, y-40, x+130, y-62,
                    1, "Pilot ID", 0, FTGACARSWidget,
                    xpWidgetClass_Caption);
    FlightNumCaption = XPCreateWidget(x+80, y-70, x+130, y-92,
                    1, "Flight No.", 0, FTGACARSWidget,
                    xpWidgetClass_Caption);
    FlightRegCaption = XPCreateWidget(x+80, y-100, x+130, y-122,
                    1, "Aircraft Reg.", 0, FTGACARSWidget,
                    xpWidgetClass_Caption);

    PIDEdit = XPCreateWidget(x+140, y-40, x+200, y-62,
                    1, "0", 0, FTGACARSWidget,
                    xpWidgetClass_TextField);
    FlightNumEdit = XPCreateWidget(x+140, y-70, x+200, y-92,
                    1, "0", 0, FTGACARSWidget,
                    xpWidgetClass_TextField);
    FlightRegEdit = XPCreateWidget(x+140, y-100, x+200, y-122,
                    1, "0", 0, FTGACARSWidget,
                    xpWidgetClass_TextField);
    XPSetWidgetProperty(PIDEdit, xpProperty_TextFieldType, xpTextEntryField);
    XPSetWidgetProperty(PIDEdit, xpProperty_Enabled, 1);
    XPSetWidgetProperty(FlightNumEdit, xpProperty_TextFieldType, xpTextEntryField);
    XPSetWidgetProperty(FlightNumEdit, xpProperty_Enabled, 1);
    XPSetWidgetProperty(FlightRegEdit, xpProperty_TextFieldType, xpTextEntryField);
    XPSetWidgetProperty(FlightRegEdit, xpProperty_Enabled, 1);
    // Register our widget handler
    XPAddWidgetCallback(FTGACARSWidget, FTGACARSHandler);

}


// This is the handler for our widget
// It can be used to process button presses etc.
// In this example we are only interested when the close box is pressed
int FTGACARSHandler(
                        XPWidgetMessage         inMessage,
                        XPWidgetID              inWidget,
                        intptr_t                inParam1,
                        intptr_t                inParam2)
{    

    // Close button will get rid of our main widget
    // All child widgets will get the bullet as well
    if (inMessage == xpMessage_CloseButtonPushed)
    {
        if (MenuItem1 == 1)
        {
            XPHideWidget(FTGACARSWidget);
        }
        XPGetWidgetDescriptor(PIDEdit, gPID, sizeof(gPID));
        XPGetWidgetDescriptor(FlightNumEdit, gFlightNum, sizeof(gPID));
        XPGetWidgetDescriptor(FlightRegEdit, gFlightReg, sizeof(gPID));
        return 1;
    }

    return 0;
}                       

float   MyFlightLoopCallback(
                                   float                inElapsedSinceLastCall,    
                                   float                inElapsedTimeSinceLastFlightLoop,    
                                   int                  inCounter,    
                                   void *               inRefcon)
{
    /* The actual callback
     */
    float   nextCallbackDuration = 1.0f;
    int     movement = 0;
    int     debounceCounter = 0;
    float   fElevation = XPLMGetDataf(gPlaneEl);
    float   fLat = XPLMGetDataf(gPlaneLat);
    float   fLon = XPLMGetDataf(gPlaneLon);
    
    gCurrentState = gNewState;
    /* Current State tasks happen here */
    switch (gCurrentState) {
        case INIT:
            gLandingVVI = 0.0f;
            gLandingIAS = 0.0f;
            gGndSpeed = 0.0f;
            gMaxElevation = 0.0f;
            gCruiseElevation = 0.0f;
            gCruiseMach = 0.0f;
            gGateStartTimer = 0.0f;
            gGateStopTimer = 0.0f;
            gAirborneStartTimer = 0.0f;
            gAirborneStopTimer = 0.0f;
            gDistance = 0.0f;
            gFuelUsed = 0.0f;
            gMaxElevation = 0.0f;
            gDebounceCounter = 0;
            gAverageSamples = 0;
            gPrevLat = fLat;
            gPrevLon = fLon;
            break;
        case IDLE:
            /* movement = (gPrevLat != fLat) && 
                (gPrevLon != fLon); */
            movement = (!AreEnginesStopped() && (XPLMGetDataf(gPlaneParkBrake)<0.9));
            break;
        case START:
            gGateStartTimer = XPLMGetDataf(gPlaneZulu);
            gDepartureLocalTime = XPLMGetDataf(gPlaneLocalTimeSeconds);
            FindNearestAirport(fLat, fLon, gsDepartureICAO);
            break;
        case GATE_DEP:
            break;
        case TAKEOFF:
            gAirborneStartTimer = XPLMGetDataf(gPlaneZulu);
            gAirborneLocalTime = XPLMGetDataf(gPlaneLocalTimeSeconds);
            break;
        case CLIMB:
            if (XPLMGetDataf(gPlaneYAGL) > 3048) nextCallbackDuration = 10.0f;
            debounceCounter = DebounceElevation(fElevation);
            break;
        case CRUISE:
            if (XPLMGetDataf(gPlaneYAGL) > 3048) nextCallbackDuration = 10.0f;
            gCruiseElevation += fElevation/10;
            gCruiseMach += MachfromTAS(fElevation, XPLMGetDataf(gPlaneTAS));
            debounceCounter = DebounceElevation(fElevation);
            gAverageSamples++;
            break;
        case UNDEFINED:
            if (XPLMGetDataf(gPlaneYAGL) > 3048) nextCallbackDuration = 10.0f;
            debounceCounter = DebounceElevation(fElevation);
            break;
        case DESCEND:
            if (XPLMGetDataf(gPlaneYAGL) > 3048) nextCallbackDuration = 10.0f;
            break;
        case APPROACH:
            gLandingVVI = XPLMGetDataf(gPlaneVVI);
            gLandingIAS = XPLMGetDataf(gPlaneIAS);
            nextCallbackDuration = 0.5f;
            break;
        case TOUCHDOWN:
            gAirborneStopTimer = XPLMGetDataf(gPlaneZulu);
            gTouchDownLocalTime = XPLMGetDataf(gPlaneLocalTimeSeconds);
            nextCallbackDuration = 1.0f;
            break;
        case GATE_ARR:
            break;
        case SHUTDOWN:
            gGateStopTimer = XPLMGetDataf(gPlaneZulu);
            gShutdownLocalTime = XPLMGetDataf(gPlaneLocalTimeSeconds);
            FindNearestAirport(fLat, fLon, gsDestinationICAO);
            /* Deactivate callback on shutdown */
            return 0;
    }
    
    /* State transitions happen here */
    switch (gCurrentState) {
        case INIT:
            gNewState = IDLE;
            break;
        case IDLE:
            /* Identify gate departure based on aircraft movement */
            if (movement) gNewState = START;
            break;
        case START:
            gNewState = GATE_DEP;
            break;
        case GATE_DEP: 
            /* Identify takeoff based on landing gear forces */
            if (XPLMGetDataf(gPlaneGearForces) == 0.0f) gNewState = TAKEOFF;
            break;
        case TAKEOFF:
            gNewState = CLIMB;
            break;
        case CLIMB:
            if (debounceCounter >= 32) gNewState = CRUISE;
            else if (XPLMGetDataf(gPlaneVVI)<=0) gNewState = UNDEFINED;
            break;
        case CRUISE:
            if (debounceCounter < 32) gNewState = UNDEFINED;
            break;
        case UNDEFINED:
            if (debounceCounter >= 32) gNewState = CRUISE;
            else if ((gMaxElevation - fElevation) >= 200) gNewState = DESCEND;
            else if (XPLMGetDataf(gPlaneVVI)>0) gNewState = CLIMB;
            break;
        case DESCEND:
            if (XPLMGetDataf(gPlaneYAGL) < 16) gNewState = APPROACH;
            break;
        case APPROACH:
            if (XPLMGetDataf(gPlaneGearForces) != 0.0f) gNewState = TOUCHDOWN;
            break;
        case TOUCHDOWN:
            gNewState = GATE_ARR;
            break;
        case GATE_ARR:
            if (AreEnginesStopped()) gNewState = SHUTDOWN;
            break;
        case SHUTDOWN:
            break;
    }
        
    if (gCurrentState != INIT) {
        if (gCurrentState != IDLE) {
            /* Write periodic position updates (Elapsed time, latitude, longitude, elevation)
               to file for debug */
            fprintf(gOutputFile, "%f,%f,%f,%f,%s,%f,%f\n",XPLMGetDataf(gPlaneLocalTimeSeconds), fLat, fLon, fElevation, enumStrings[gCurrentState], gCruiseElevation/gAverageSamples, gCruiseMach/gAverageSamples);
        }
        gDistance += gGndSpeed * inElapsedSinceLastCall;
        gFuelUsed += CalculateFuelBurn(gFuelFlow) * inElapsedSinceLastCall;
        gGndSpeed = XPLMGetDataf(gPlaneGndSpeed);
        /* Populate fuel flows array */
        XPLMGetDatavf(gPlaneFuelFlow, gFuelFlow, 0, 9);  
    }
       
    return nextCallbackDuration;
}                                   


